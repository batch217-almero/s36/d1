// CONTAINS ALL SCHEMA FOR THE APP
const mongoose = require("mongoose");

// SCHEMA FOR TASKS
const taskSchema = new mongoose.Schema({
	name: {
		type: String,
		require: [true, "Name is required for this action"]
	}, 
	status: {
		type: String,
		default: "pending"
	}
});

// EXPORT SCHEMA TO OTHER FILES
module.exports = mongoose.model("Task", taskSchema);