// SETUP DEPENDENCIES/MODULES
const express = require("express");
const mongoose = require("mongoose");

// CUSTOM FILES DEPENDENCIES
const taskRoute = require("./routes/taskRoute.js")


// SERVER SETUP
const app = express();
const port = 3001;

// ADDITIONAL SETTINGS/MIDDLEWARES
app.use(express.json());
app.use(express.urlencoded({extended:true}));


// ADD MONGODB CONNECTION
mongoose.connect("mongodb+srv://admin:admin123@zuitt.vd4owhx.mongodb.net/B217_to-do?retryWrites=true&w=majority", {
	useNewUrlParser:true, 
	useUnifiedTopology:true
});

// CUSTOM MIDDLEWARE
// localhost:3001/tasks
app.use("/tasks", taskRoute);

// APP LISTENER
app.listen(port, () => console.log(`Now listening to port ${port}`));
