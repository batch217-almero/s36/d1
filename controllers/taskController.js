// REQUIRE A FILE TO BE USED IN THIS FILE
const Task = require("../models/task.js")

module.exports.getAllTasks = () => {
	return Task.find({}).then(result =>{
		return result;
	});
}

module.exports.createTask = (requestBody) =>{
	let newTask = new Task({
		name: requestBody.name
	})

	return newTask.save()
	.then((task, error) =>{
		if (error) {
			console.log(error);
			return false;
		} else{
			return task;
		}
	})
}

module.exports.deleteTasks = (taskID) => {
	return Task.findByIdAndRemove(taskID)
	.then((removedTask, err) => {
		if(err){
			console.log(err);
			return false;
		}else{
			return removedTask;
		}
	})
}

module.exports.updateTask = (taskId, newContent) => {
	return Task.findById(taskId)
	.then((result, error) =>{
		if(error){
			console.log(error);
			return false;
		}

		result.name = newContent.name;
		return result.save()
		.then((updateTask, saveError) =>{
			if(saveError){
				console.log(saveErr);
				return false;
			}else{
				return updateTask;
			}
		})
	});
}

// ACTIVITY-------------------------------------------------------------
// STEP 2: Create a controller function for retrieving a specific task.
module.exports.viewSpecificTask = (taskId) => {
	return Task.findById(taskId)
	.then(result => {
		// STEP 3: Return the result back to the client/Postman.
		return result;
	});
}

// STEP 6: Create a controller function for changing the status of a task to "complete".
module.exports.changeStatus = (taskId) => {
	return Task.findById(taskId)
	.then((result, error) =>{
		if (error) {
			console.log(error);
			return false;
		}

		result.status = "complete";
		return result.save()
		.then((updateTask, saveError) =>{
			if (saveError) {
				console.log(saveErr);
				return false;
			}else{
				// STEP 7: Return the result back to the client/Postman.
				return updateTask;
			}
		})
	});
}