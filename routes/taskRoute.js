// CONTAINS ALL ENDPOINTS FOR THE APP

// ROUTE DEPENDENCIES
const express = require("express");
const router = express.Router();

// ADD TaskController to REQUIRE
const taskController = require("../controllers/taskController.js");

// CREATE ENDPOINTS USING ROUTE
router.get("/viewTasks", (request, response) => {
	taskController.getAllTasks()
	.then(resultFromController => response.send(resultFromController));
});

router.post("/addNewTasks", (request, response) => {
	taskController.createTask(request.body)
	.then(resultFromController => response.send(resultFromController));
});

router.delete("/deleteTask/:id", (request, response) => {//":id" means wildcard
	taskController.deleteTasks(request.params.id)
	.then(resultFromController => {
		response.send(resultFromController);
	})
});

router.put("/updateTasks/:id", (request, response) => {
	taskController.updateTask(request.params.id, request.body)
	.then(resultFromController => response.send(resultFromController));
});


// ACTIVITY-------------------------------------------------------------
// STEP 1: Create a route for getting a specific task.
router.get("/specificTask/:id", (request, response) => {
	taskController.viewSpecificTask(request.params.id)
	.then(resultFromController => {
		response.send(resultFromController)
	});
});

// STEP 5: Create a route for changing the status of a task to "complete".
router.put("/changeStatus/:id/complete", (request, response) => {
	taskController.changeStatus(request.params.id)
	.then(resultFromController => {
		response.send(resultFromController);
	})
});



// EXPORT ROUTER OBJECT TO OTHER FILES
module.exports = router;